import logging

#from app_logging import DesiLogger

from connection import Connection as CONN

# all the queries are here
logging.basicConfig(filename='/Users/jumbotail/automated_scripts/ImrCancelleation/imr_cancellation.log', filemode='a',
                    format='%(asctime)s - %(levelname)s - %(message)s')
class SendQuery:

    def __init__(self, list1):
        self.theList = list1
        #self.loggy = DesiLogger()

    def call_executor(self):
        # query_1: get ids for all imrs

        if len(self.theList) >1:
            get_imr_id = (
            'select id from inventory_item_movement_request where natural_id in{0}'.format(tuple(self.theList, )))
        else:
            get_imr_id = f'select id from inventory_item_movement_request where natural_id = "{self.theList[0]}"'

        print(get_imr_id)

        connector = CONN()
        result_set_1 = connector.connect(get_imr_id)
        print("all_query returned a True")
        next_query_set = []
        for i in result_set_1:
            next_query_set.append(i[0])
        print(str(result_set_1))
        print(result_set_1)
        print(next_query_set)
        # query_2: get step_request_id, transfer_job_id, execution_id

        if len(next_query_set) > 1:
            get_through_imr_id = (
            'select id, job_execution_step_id, transfer_job_id from transfer_job_step_request where inventory_movement_request_id in {0};'.format(
                tuple(next_query_set, )))
        else:
            get_through_imr_id = f'select id, job_execution_step_id, transfer_job_id from transfer_job_step_request where inventory_movement_request_id = {next_query_set[0]};'
        print(get_through_imr_id)
        connector = CONN()
        result_set_2 = connector.connect(get_through_imr_id)
        print(str(result_set_2))
        print(result_set_2)

        step_request_id = []
        job_exec_step_id = []
        transfer_job_id = []
        for i in result_set_2:
            step_request_id.append(i[0])
            job_exec_step_id.append(i[1])
            transfer_job_id.append(i[2])

        step_req_next = []
        job_exec_next = []
        transfer_job_next = []

        for i in step_request_id:
            if i not in step_req_next:
                step_req_next.append(i)

        for i in job_exec_step_id:
            if i not in job_exec_next:
                job_exec_next.append(i)

        for i in transfer_job_id:
            if i not in transfer_job_next:
                transfer_job_next.append(i)

        print('\nstep_req_next:', step_req_next, '\njob_exec_next:', job_exec_next, '\ntransfer_job_next:',
              transfer_job_next)

        prev_state = 'CREATED'

        new_state = 'CANCELLED'

        if len(next_query_set) > 1:
            cancel_imr = '''update `inventory_item_movement_request` set status = (case when status = 'CREATED' then 'CANCELLED' else status end) where id in {0};'''.format(tuple(next_query_set, ))
            select_imr = '''select status from inventory_item_movement_request where id in {0};'''.format(tuple(next_query_set,))
        else:
            cancel_imr = f'''update `inventory_item_movement_request` set status = (case when status = {prev_state} then {new_state} else status end) where id = {next_query_set[0]};'''
            select_imr = f'''select status from inventory_item_movement_request where id = {next_query_set[0]};'''
        print(cancel_imr)
        print(select_imr)
        imr_cancel = connector.connect(cancel_imr)
        print(imr_cancel)

        if len(step_req_next) > 1:
            cancel_step_req = '''update `transfer_job_step_request` set status = (case when status = 'CREATED' then 'CANCELLED' else status end) where id in {0};'''.format(tuple(step_req_next, ))
            select_step = '''select status from transfer_job_step_request where id in {0};'''.format(
                tuple(step_req_next, ))
        else:
            cancel_step_req = f'''update `transfer_job_step_request` set status = (case when status = {prev_state} then {new_state} else status end) where id = {step_req_next[0]};'''
            select_step= f'''select status from transfer_job_step_request where id in {step_req_next[0]};'''
        print(cancel_step_req)
        print(select_step)
        step_cancel_res = connector.connect(cancel_step_req)
        print(step_cancel_res)

        if len(job_exec_next) > 1:
            cancel_job_exec = '''update `transfer_job_execution_step` set job_execution_step_status = (case when job_execution_step_status = 'CREATED' then 'CANCELLED' else job_execution_step_status end) where id in {0};'''.format(tuple(job_exec_next, ))
            select_exec = '''select job_execution_step_status from transfer_job_execution_step where id in {0};'''.format(
                tuple(step_req_next, ))
        else:
            cancel_job_exec = f'''update `transfer_job_execution_step` set job_execution_step_status = (case when job_execution_step_status = {prev_state} then {new_state} else job_execution_step_status end) where id = {job_exec_next[0]};'''
            select_exec = f'''select job_execution_step_status from transfer_job_execution_step where id in {job_exec_next[0]};'''
        print(cancel_job_exec)
        print(select_exec)
        jexec_cancel_res = connector.connect(cancel_job_exec)
        print(jexec_cancel_res)

        if len(transfer_job_next) > 1:
            cancel_transfer_job = '''update `transfer_job` set job_status = (case when job_status = 'CREATED' then 'CANCELLED' else job_status end) where id in {0};'''.format(tuple(transfer_job_next,))
            select_tj = '''select job_status from transfer_job where id in {0};'''.format(tuple(transfer_job_next,))
        else:
            cancel_transfer_job = f'''update `transfer_job` set job_status = (case when job_status = {prev_state} then {new_state} else job_status end) where id = {transfer_job_next[0]};'''
            select_tj = f'''select job_status from transfer_job  where id = {transfer_job_next[0]};'''
        print(cancel_transfer_job)
        print(select_tj)
        tj_cancel_res = connector.connect(cancel_transfer_job)
        print(tj_cancel_res)


        return True
