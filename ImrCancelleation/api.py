import flask
from flask import jsonify, request

from all_query import SendQuery

# from app_logging import Logger
# import flask.ext.restful.reqparse.RequestParser as reqparser


app = flask.Flask(__name__)
app.config["DEBUG"] = True


# logg = Logger()

@app.route('/', methods=['GET'])
def home():
    return "<h1>Distant Reading Archive</h1>YOU ARE HERE FOR IMR CANCELLATION, POST it</p>"


@app.route('/cancel-whole-job', methods=['POST'])
def addOne():
    req_data = request.get_json()
    mess = req_data['imrs']
    print(mess)
    imr_list = [mess]
    try:
        query_body = SendQuery(mess)
        flag = query_body.call_executor()
        if flag:

            # logg.info(f'{imr_list}')
            print("Success")
            return jsonify({'cancelled Job with IMRs': imr_list})
        else:
            print("failed")
    except:
        return jsonify({'Aborted Operation, all_query returned False'})
        logg.info("all_query returned a False")

    return jsonify({'cancelled Job with IMRs': imr_list})
    imr_list = []


def run_api():
    app.run(host='0.0.0.0', port=80)
