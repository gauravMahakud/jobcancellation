import logging
import sys


import pymysql


class Connection:

    def __init__(self):

        logging.info("In Connection")

        self.rds_host = "jt-rds-prod.ce6lasmdz6oo.ap-southeast-1.rds.amazonaws.com"
        self.name = "--" #add userName
        self.password = "--" #password
        self.db_name = "jt_jobs_prod"
        self.port = 4008

    def connect(self, query):

        try:
            conn = pymysql.connect(self.rds_host, user=self.name, passwd=self.password, db=self.db_name, port=self.port, autocommit= True)
            print("Connecting...")
            cursor = conn.cursor()
            print("connection established")
            print("Connection established.")
            cursor.execute("select 1;")
            print("Select query Working, executing the actual query below")
            cursor.execute(query)
            result = cursor.fetchall()
            conn.close()
            return result

        except:
            print("ERROR: Unexpected error: Could not connect to MySql instance.")
            print("Closing connection and exiting")
            sys.exit()
